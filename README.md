# Un homme sans rêve

Ce dépôt contient la nouvelle *Un homme sans rêve*. Il s'agit à la base d'un
chapitre du bouquin *Asmara* que j'avais écrit lors du NaNoWriMo 2014 mais que
je n'ai jamais réussi à finaliser. Comme je souhaitais tout de même publier
quelque chose, j'ai donc extrait ce chapitre puis je l'ai retravaillé. De 16e
chapitre, il est possible (mais peu probable) que cette nouvelle serve de base
pour une réécriture d'*Asmara*.

*Un homme sans rêve* est une réinterprétation d'un des chapitres du livre
*1984* de Georges Orwell (dont je vous conseille bien évidemment la lecture :))
; la trame principale est donc assez similaire. C'était aussi un exercice de
style pour travailler les dialogues et l'évolution psychologique du personnage
principal. Ne vous formalisez donc pas pour les ressemblances !

Aussi, si vous êtes intéressés par le processus d'écriture et de relecture de
cette nouvelle, vous pouvez [visualiser l'historique des changements](https://framagit.org/marien.fressinaud/un-homme-sans-reve/commits/master).
La première version ne diffère pas tant que ça de la version finale et a été
écrite d'un trait mais je trouvais tout de même qu'il était intéressant de «
libérer » ce processus aussi.

---

Le texte est écrit en Markdown et un PDF est généré à partir de la ligne de
commande suivante :

```sh
$ pandoc Un_homme_sans_reve.md -f markdown_mmd -t latex -o Un_homme_sans_reve.pdf
```
