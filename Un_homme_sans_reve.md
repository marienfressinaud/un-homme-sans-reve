Title: Un homme sans rêve  
Date: 2018-05-30  
Author: Marien Fressinaud  
License: CC-BY

Thomas ignorait où il se trouvait. Probablement dans l’une des Trois Sœurs, ces immenses bâtiments appartenant au Pouvoir, mais il n’avait aucun moyen de s’en assurer. Une fois ses muscles immobilisés, il avait perdu connaissance et s’était ensuite réveillé dans cette minuscule salle éblouissante. Il s’était assis sur la banquette sur laquelle il avait repris connaissance un peu plus tôt. À l’autre bout de la pièce, un trou au sol devait probablement servir de toilettes.

Partageait sa cellule avec lui, un Agent. Thomas n’en avait encore jamais vu de pareil, il s’agissait probablement d’une série dédiée à la surveillance des prisonniers. La lumière qui émanait de son œil était d’un bleu bien plus glacial que tout ce qu’il avait vu jusque-là. Il se tenait droit, debout sur ses jambes métalliques. Son allure et sa posture rappelait celle d’un humain tant et si bien qu’au réveil, Thomas l’avait pris pour un banal gardien fait d’os et de chair. Il était fin, grand et même beau, pour un robot. Toute la carcasse de celui-ci avait été ensevelie sous une cuirasse qui réfléchissait la lumière froide provenant de la pièce.

Les vêtements du jeune homme avait été changés, ils sentaient désormais le propre et le sec. Dans son ventre grognait toutefois la faim terrible et tenace. Il n’avait plus ni mangé ni bu depuis des heures, probablement des jours, il ne savait pas. Néanmoins, il était certain que maintenant qu’il était réveillé, on viendrait le nourrir. Le Pouvoir prendrait soin de lui. Il attendit ainsi plusieurs minutes qu’on vienne le chercher sans qu’aucun bruit n’émane ni de lui, ni du robot immobile. La salle était plongée dans le silence le plus profond et Thomas attendait dedans que quelque chose se passe.

Il pensa un temps à Abie qui avait totalement disparu. Elle n’avait laissé aucun indice en partant&nbsp;; si seulement ils avaient gardé cette fichue puce de localisation dans leur bras&nbsp;! Le seul signe qu’il avait pu percevoir d’elle était cette rose blanche qui poussait dans le Tunnel. Cette toute petite rose blanche, si étrange, qui avait réussi à survivre dans ce milieu si hostile. Elle poussait là, en-bas, peut-être même sous ses pieds, plusieurs mètres plus bas. La rose blanche poussait dans le Tunnel comme elle poussait dans le cou d’Abie&nbsp;; la rose blanche, son tatouage. Il n’avait pas eu l’occasion de lui demander sa signification et il finit par arrêter de se torturer l’esprit pour cela.

Le temps tournait et il ne savait toujours pas quelle heure du jour ou de la nuit il était. L’Agent qui le surveillait restait immobile, seule la lumière bleue de son œil montrait qu’il était activé. Il n’y avait aucune fenêtre dans sa cellule et la porte de métal froid qui lui faisait face restait invariablement fermée. La faim quant à elle continuait inlassablement de lui tirailler l’estomac. Sa gorge, sèche désormais, réclamait de l’eau. Il en vint à regretter de ne pas avoir gardé ses vêtements trempés. De l’eau, il y en avait tellement qui tombait du ciel au-dehors.

Thomas se leva pour se dégourdir les jambes. Il se dirigea d’abord vers le trou qui semblait s’enfoncer profondément dans le sol, aucune chasse d’eau n’était visible et une odeur fétide s’en dégageait. Qui pouvait bien avoir envie de se soulager dans cette ignoble ouverture&nbsp;? Il se dirigea ensuite vers la porte pour l’examiner mais alors qu’il passait devant l’Agent pour s’en approcher, celui-ci le rappela à l’ordre&nbsp;:

«&nbsp;Détenu Kell, veuillez vous tenir éloigné de la porte.&nbsp;»

Thomas ne se fit pas prier&nbsp;: ce n’était pas le moment de se faire mal voir du Pouvoir. Il savait maintenant exactement ce qu’il allait faire pour amadouer ses membres et se racheter. Il avait en effet mémorisé un certain nombre de noms de personnes travaillant au sein même des Trois Sœurs et qui avaient voué leur cause à la résistance. Il les ferait tomber un à un. Il connaissait maintenant aussi les noms des principaux protagonistes de la résistance qui tiraient les ficelles à partir de la ville sous la Ville. Il les ferait tomber eux aussi. Thomas ne ressentait aucune empathie pour ces hommes qui se cachaient sous des principes douteux afin de déclencher une guerre civile et faire tomber la paix qui régnait aujourd’hui dans Asmara. Le Pouvoir les ferait taire et tout cela grâce à lui.

Soudain, le robot s’anima de nouveau.

«&nbsp;Détenu Kell, veuillez vous tenir immobile sur votre lit.&nbsp;»

Si tant est que l’on pouvait appeler ça un lit, Thomas se rassit sur la banquette attendant qu’on vienne le chercher. La porte se déverrouilla de l’extérieur et s’ouvrit sur un second Agent du même type que celui présent dans la cellule. Le jeune homme le regarda s’approcher de lui, attendant qu’il lui dise quoi faire.

Le coup partit sans qu’il ne le vit venir. Le bras mécanique venait de le frapper au niveau de l’épaule. La douleur lui inonda l’ensemble du bras et l’abasourdit le temps que le robot l’empoigne vigoureusement et le mette debout. Il le força ensuite à avancer pour sortir de la cellule et ils rejoignèrent un long couloir entièrement blanc. Alors qu’ils avançaient, les portes se succédaient de chaque côté les unes après les autres, toutes identiques. Thomas songea qu’il devait s’agir d’autres cellules pour détenus et se demanda si chacune d’entre elles était habitée. «&nbsp;Impossible, se prit-il à penser, le Pouvoir ne peut avoir autant d’ennemis&nbsp;».

Alors qu’il traversait le couloir accompagné des deux Agents, il aperçut un vieil homme tout habillé de blanc au fond du couloir. En apercevant Thomas et les deux Agents, il s’arrêta un instant pour les dévisager et marmonna des sons inaudibles. Derrière l’homme apparurent à leur tour deux autres Agents et l’un deux frappa violemment le dos de la personne qui s’effondra à terre. Thomas discerna des sanglots étouffés mais ce n’était pas ses affaires&nbsp;: les crimes des autres ne le concernait pas. Il n’était déjà pas tout blanc et devait par conséquent se racheter. Il continua d’entendre l’homme pendant un petit moment avant que l’Agent qui l’avait frappé une première fois ne le fasse taire définitivement en l’assomant.

Thomas et ses gardiens arrivèrent bientôt dans une autre pièce encore plus éblouissante ici que n’importe où d’autre dans Asmara, la salle baignait littéralement dans la lumière. La douleur dans l’épaule de Thomas continuait de l’élancer et le robot qui le maintenait ne faisait rien pour le ménager. L’Agent le força à aller s’asseoir sur une chaise de verre placée dans un léger renfoncement du sol et située exactement au centre de la pièce ronde. Autour d’eux se trouvait une sorte d’immense ordinateur circulaire qui faisait le tour de la salle. Tout était parfaitement harmonieux ici. Une voix robotique sortie de nul part envahit alors la salle.

«&nbsp;Détenu Thomas Kell, vous allez être jugé pour les crimes suivants&nbsp;: couverture d’un meurtre sur personne appartenant au Pouvoir, tentative de corruption des données relatives à votre arrestation, fuite devant Agents, propagande de mensonges liés à la résistance et retrait de votre bras puis destruction d’une puce d’identification appartenant au Pouvoir. Il ne sera entendu aucun élément de défense de votre part. Vous êtes jugé coupable.  
—&nbsp;Attendez&nbsp;!&nbsp;» s’écria Thomas.

Mais déjà un Agent lui frappait l’épaule droite. La douleur se réveilla et s’amplifia tel un ballon que l’on aurait gonflé. Il était jugé coupable sans avoir pu se défendre, c’était inconcevable&nbsp;! Il devait se faire entendre mais le robot se maintenait tout près de lui, lui coupant toute envie de velléité. Ils s’étaient forcément trompés, ils ne pouvaient pas le juger de cette manière.

Entra alors par une autre porte un homme en blouse blanche. Il était grand, bien bâti&nbsp;; sa posture droite lui donnait de l’assurance. Son visage surmonté d’une fine paire de lunettes rayonnait de bienveillance comme jadis celui de Heath Tims. Heath Tims, celui depuis lequel tout était parti. Il faisait lui-même partie de la résistance et avait sans doute voulu l’attirer à se rebeller. L’homme qui se maintenait désormais devant lui le regardait avec sympathie.

«&nbsp;Bonjour Thomas&nbsp;», dit-il simplement.

Thomas resta muet ne sachant s’il fallait répondre.

«&nbsp;Thomas, je suis ici pour vous aider.  
—&nbsp;P… pour m’aider&nbsp;? Allez-vous me juger&nbsp;?&nbsp;»

L’homme eut un léger sourire.

«&nbsp;Vous juger&nbsp;? Non, bien sûr que non. N’avez-vous pas prêté attention&nbsp;? Vous avez déjà été jugé Thomas.  
—&nbsp;Mais la résistance&nbsp;! Je peux vous donner tous les noms que j’ai en ma possession&nbsp;!  
—&nbsp;La résistance&nbsp;? répéta l’homme calmement. Mais quelle résistance&nbsp;?&nbsp;»

Thomas resta bouche-bée.

«&nbsp;Savez-vous qui je suis&nbsp;?&nbsp;» continua l’homme aux lunettes.

Il n’en avait aucune idée.

«&nbsp;Je suis le docteur O’Connor. Je travaille pour le Pouvoir depuis… et bien depuis plus longtemps que vous ne pouvez l’imaginer.&nbsp;»

O’Connor&nbsp;? Avait-il bien dit qu’il s’appelait «&nbsp;O’Connor&nbsp;»&nbsp;?

«&nbsp;Je vois à votre regard que vous avez fait le lien n’est-ce pas&nbsp;?&nbsp;»

Thomas acquiesca légèrement de la tête. Ce nom, il était certain l’avoir vu comme faisant parti de la résistance. Était-il ici pour l’aider&nbsp;? Rien n’était moins sûr maintenant qu’il lui avait proposé de donner les noms au Pouvoir. Quel que soit le camp dans lequel pouvait se trouver O’Connor, il n’avait aucune raison de vouloir l’aider désormais.

L’homme le regarda avec un sourire amusé puis reprit tranquillement.

«&nbsp;Savez-vous pourquoi vous vous trouvez ici&nbsp;?&nbsp;»

Thomas n’avait aucune raison de mentir ou de jouer un quelconque jeu désormais.

«&nbsp;Pour avoir trahi la confiance du Pouvoir.  
—&nbsp;Non.  
—&nbsp;Pour avoir douté alors&nbsp;?  
—&nbsp;Non non, cherchez encore.&nbsp;»

Thomas n’avait aucune idée de ce qu’il sous-entendait.

«&nbsp;Vous êtes malade Thomas, tout simplement, répondit O’Connor à sa place. Vous êtes malade et nous devons vous soigner. Vous êtes sujet à des crises qui vous poussent à vous rebeller face au Pouvoir.  
—&nbsp;Bien sûr que non, j’ai parfaitement conscience de ce que j’ai fait. J’ai conspiré et je veux me repentir. Je ferai ce que m’ordonnera le Pouvoir de faire&nbsp;!&nbsp;»

L’homme le regardait avec des yeux perçants à travers ses lunettes.

«&nbsp;Comment souhaitez-vous vous repentir Thomas&nbsp;? Comment le pourriez-vous&nbsp;? Pensiez-vous qu’une liste de noms montée de toute pièce par nos services suffirait à vous racheter&nbsp;?&nbsp;»

Thomas fut suffoqué par la révélation.

«&nbsp;Croyiez-vous réellement que des résistants tiennent une liste de leurs membres infiltrés&nbsp;? Quel genre d’individus irresponsables auraient-ils donc été&nbsp;?  
—&nbsp;Je… Je ne sais pas&nbsp;», dut admettre Thomas qui trouvait désormais l’idée bien saugrenue.

O’Connor avait haussé la voix, prenant un air sévère comme s’il était en train de gronder un enfant qui avait volé des bonbons. Et comme Thomas semblait avoir saisi la leçon, l’homme en blouse se radoucit.

«&nbsp;Nous ne vous en voulons pas Thomas. Vous pourriez penser que nous souhaiterions vous punir par un quelconque moyen mais ce ne sera pas le cas. Nous aimerions au contraire vous soigner car ce n’est pas vous qui étiez aux commandes&nbsp;; vous n’étiez pas maître de ce qu’il se passait ici.&nbsp;»

L’homme venait de lui tapoter légèrement le haut du crâne.

«&nbsp;Qu’avez-vous donc vu exactement en-bas&nbsp;? continua O’Connor.  
—&nbsp;Des hommes et des femmes.  
—&nbsp;Qui étaient-ils&nbsp;?  
—&nbsp;Ils se désignaient comme des résistants. Ils disaient lutter contre le Pouvoir. Ils pensaient pouvoir rendre la ville d’Asmara plus humaine&nbsp;», répondit lentement Thomas.

O’Connor soupira.

«&nbsp;Plus humaine… Pourquoi pensez-vous donc qu’Asmara nécessite plus d’humanité que ce qu’elle ne possède déjà&nbsp;?  
—&nbsp;Mais ce n’était pas moi&nbsp;!&nbsp;» s’exclama Thomas.

L’homme secoua la tête comme attristé.

«&nbsp;Il n’y a jamais eu de résistance Thomas.&nbsp;»

Thomas regarda O’Connor, interloqué.

«&nbsp;Il n’y a jamais eu de résistance et il n’y en aura très probablement jamais. Ce que vous avez pu voir n’était que le fruit de votre imagination, rien de plus. Vous avez imaginé des personnes auxquelles vous vous identifiiez. Ce que vous pensez avoir entendu n’était que l’echo d’un cerveau malade.&nbsp;»

Thomas ne savait plus quoi répondre. Il était certain de ce qu’il avait vécu ces dernières semaines, cela ne pouvait être le fruit de son imagination.

«&nbsp;Vous êtes malade Thomas, reprit O’Connor, mais rassurez-vous, vous serez bientôt soigné.&nbsp;»

L’homme fit un signe aux Agents qui se trouvaient quelques mètres derrière. Ils s’approchèrent, attrapèrent le jeune homme pour le soulever et l’attachèrent fermement à une sorte de table verticale qui venait de prendre la place de la chaise en verre. Les sangles lui saignaient la peau. Thomas ne comprenait plus du tout ce qui lui arrivait. Il ne voulait pas croire non plus ce que lui disait O’Connor. Il avait vu ces gens, il avait vu les bateaux partir, il avait senti les odeurs, entendu les cris. Tout cela il n’avait pu l’inventer. Et tandis que les Agents lui installaient des électrodes sur le sommet du crâne, O’Connor le regardait toujours d’un petit air triste mais bienveillant.

«&nbsp;Attendez O’Connor, je peux vous servir, je soutiens le Pouvoir, je l’ai toujours soutenu&nbsp;!  
—&nbsp;Bien Thomas, ce sont ici de belles paroles, mais le pensez-vous réellement&nbsp;?  
—&nbsp;Évidemment&nbsp;!&nbsp;», s’écria Thomas.

L’homme fit un son avec sa bouche en signe de désapprobation.

«&nbsp;Vous êtes dans le faux depuis des années Thomas. Vous êtes persuadés être un jeune homme talentueux alors que votre travail n’est que médiocrité. Vous vous sentez au-dessus de tout le monde dans tous les domaines. Avez-vous gouté les joies de la défaite lors de votre dernière partie de *Speed Party*&nbsp;? Enfermé dans vos certitudes, vous ne pensiez probablement pas vous faire battre&nbsp;?  
—&nbsp;C’était vous&nbsp;?  
—&nbsp;Non Thomas, il s’agissait de la réalité de ce que vous êtes. Celle qui s’est offerte à vous lorsque vous avez rejoint les Trois Sœurs. Votre promotion n’avait pas pour objectif de reconnaitre vos talents&nbsp;; il s’agissait de vous confronter à la réalité et de vous faire abandonner vos rêves.  
—&nbsp;Mes rêves&nbsp;?  
—&nbsp;Bien sûr, vos rêves de velléité.&nbsp;»

Thomas ne comprenait pas où voulait en venir O’Connor. Il le disait malade mais il devait aussi savoir qu’il n’en était rien. Le docteur ainsi que le Pouvoir derrière lui devaient se jouer du jeune homme attaché. Se moquaient-ils de lui pour le simple plaisir ou avaient-ils un dessein, Thomas n’en savait rien pour le moment.

«&nbsp;Nous allons désormais procéder à votre guérison. Cela pourrait prendre des mois, mais vous connaissant je parierai plutôt sur quelques heures. Tout dépendra de vous pour réduire ce temps à une poignée de minutes, vous avez toutes les cartes en main.  
—&nbsp;Qu’attendez-vous de moi&nbsp;? Que voulez-vous que je vous dise&nbsp;? questionna le jeune homme qui commençait à s’affoler.  
—&nbsp;Ce que vous nous direz nous importe peu. Ce que vous pensez en revanche…&nbsp;»

L’homme laissa planer sa dernière phrase dans les airs comme une menace.

«&nbsp;Ce que je pense&nbsp;? Comment voulez-vous que je maîtrise mes pensées&nbsp;?  
—&nbsp;Voilà déjà une bien belle preuve d’honnêteté. Continuez ainsi et vous serez bientôt libre.&nbsp;»

«&nbsp;Libre&nbsp;», le mot résonnait comme une promesse. Mais alors que Thomas se délectait de cette délicieuse perspective, une vive douleur lui traversa le corps de la tête aux pieds.

«&nbsp;Aïe&nbsp;! s’exclama Thomas.  
—&nbsp;Cela vous fait-il du bien&nbsp;? demanda O’Connor.    
—&nbsp;Du bien&nbsp;? Comment voulez-vous…&nbsp;»

Thomas n’eut pas le temps de finir sa question qu’une deuxième décharge lui traversait le corps. Celle-ci fut plus longue et eut le temps de remonter une seconde fois à la tête. Le jeune homme sanglé à la table de verre voyait désormais flou, sonné par le choc.

«&nbsp;Je vais vous poser une série de questions, vous devrez y répondre le plus honnêtement possible. À la moindre erreur ou au moindre signe de malhonnêteté de votre part, une décharge vous sera envoyée. Au moindre signe de velléité ou de mauvaise foi, la durée sera doublée. Le Pouvoir a besoin d’hommes forts et vous êtes déjà si faible Thomas, nous aimerions éviter de vous tuer.&nbsp;»

Thomas reprenait à peine ses esprits. Il sentait autour des électrodes que les Agents lui avaient installées sur les tempes une forte chaleur. Dans l’air il croyait déceler une odeur de cramé mais ses sens pouvaient tout aussi bien être déréglés.

«&nbsp;Vous disiez ne pas vouloir me punir, articula difficilement Thomas.  
—&nbsp;Vous aurez probablement imaginé cela aussi&nbsp;», lui assura tranquillement O’Connor.

L’homme leva une main, deux doigts étaient tendus.

«&nbsp;Combien de doigts&nbsp;? questionna-t-il le plus innocemment du monde.  
—&nbsp;Deux, évidemment.&nbsp;»

Une décharge le foudroya.

«&nbsp;Faux&nbsp;!&nbsp;» lui jeta O’Connor.

N’y en avait-il pas deux&nbsp;? Thomas reconsidéra la main qu’il lui tendait. Il y avait très vraisemblablement deux doigts tendus en l’air.

«&nbsp;J’en vois toujours deux, répéta Thomas.  
—&nbsp;Ce que vous pouvez bien voir ou interpréter ne me concerne pas. Si le Pouvoir vous dit que je ne tends pas deux doigts, que préférez-vous penser&nbsp;?  
—&nbsp;Ce que je vois évidemment&nbsp;! s’exclama Thomas.  
—&nbsp;Et pourquoi tenir pour acquis ce que vous voyez si l’on vous dit le contraire&nbsp;?  
—&nbsp;Parce que c’est ce que *je* vois, peu importe ce que vous voulez me faire croire.&nbsp;»

Un éclair lézarda son corps. Il sentait poindre les fourches foudroyantes le long de ses bras et de ses jambes. Piqués par la douleur, ses doigts de pieds formèrent un angle absurde. Son corps sembla vouloir se projeter en avant, immédiatement retenu par les sangles qui le clouaient à la table. Elles s’enfoncèrent encore plus profondément dans sa peau et réveillèrent la douleur induite par les coups qui lui avait donnés l’Agent.

«&nbsp;Ne faites pas de mauvais esprit Thomas. Nous n’essayons pas de vous faire croire quoi que ce soit, nous vous instruisons à la réalité. Ce que vous voyez n’est que le produit de votre maladie.  
—&nbsp;Maladie que vous m’inventez, essaya de renchérir le jeune homme d’une faible voix.  
—&nbsp;Ne jouez pas avec ma patience Thomas. Votre cas n’est pas encore désespéré, il suffit que vous y mettiez du vôtre.&nbsp;»

Thomas ne comprenait pas ce que lui voulait O’Connor. Deux doigts, il y avait deux doigts tendus. S’il donnait la réponse, celle-ci ne conviendrait pas. S’il en donnait une autre, O’Connor le traiterait de menteur et la punition en serait bien plus douloureuse.

«&nbsp;Je vais vous donner un indice car j’ai peur que vous ne vous soyez égaré. Depuis que vous êtes né, vous percevez le monde à travers votre enveloppe corporelle. Les sens perçus à travers vos yeux, vos oreilles, votre bouche, votre nez et votre peau vous ont permis de vous forger le monde dans lequel vous vivez. Derrière tout cela, votre cerveau essaye tant bien que mal de mettre de l’ordre dans l’afflux de données qui lui arrive. Que se passerait-il alors si vos capteurs sensoriels venaient à vous mentir&nbsp;?  
—&nbsp;Pourquoi le feraient-ils&nbsp;?  
—&nbsp;Vous ne répondez pas à ma question.&nbsp;»

Thomas resta silencieux quelques instants avant de répondre.

—&nbsp;J’imagine que mon cerveau interprêterait ces signaux comme n’importe quel autre.  
—&nbsp;Exact&nbsp;! s’exclama O’Connor soudainement jovial. Et il devient ainsi facile de détecter un cerveau malade car celui-ci ne fait que réagir à des signaux extérieurs.  
—&nbsp;Je ne comprends pas…  
—&nbsp;Cela va venir mais revenons-en d’abord à ma question. Vous êtes probablement persuadé que deux et deux font quatre&nbsp;: il n’en est rien car le Pouvoir vous dit qu’ils font cinq.  
—&nbsp;Cinq&nbsp;? Pourquoi deux et deux feraient-ils cinq&nbsp;? C’est absurde.  
—&nbsp;La vérité du Pouvoir serait donc absurde&nbsp;?  
—&nbsp;Elle n’a du moins pas plus de validité que la mienne.&nbsp;»

Un autre éclair foudroya Thomas. Celui-ci avait perdu le compte. Peut-être en avait-il subit quatre, voire peut-être cinq, des chiffres bien inutiles car O’Connor serait bien capable d’affirmer sans sourciller qu’il n’avait pas encore commencé la séance de torture.

«&nbsp;Savez-vous ce qu’est le Pouvoir&nbsp;? reprit l’homme. Connaissez-vous sa nature profonde, celle qui fait qu’elle ne peut pas se tromper face à vous&nbsp;?  
—&nbsp;Je l’ignore, dut admettre le jeune homme.  
—&nbsp;Le Pouvoir n’est qu’intelligence mathématique et algorithmes Thomas. Le Pouvoir ne peut se tromper sur la nature des choses car il a été créé sur les fondements mêmes de la réalité. Comprenez-vous pourquoi votre vérité ne peut être acceptée si celle-ci diverge de celle du Pouvoir&nbsp;?  
—&nbsp;Non, je dois bien admettre que non, je ne comprends pas, répondit Thomas, exténué.  
—&nbsp;Le Pouvoir Thomas, répondit O’Connor, le Pouvoir n’est que machine. La ville d’Asmara, Thomas, est dirigée par une intelligence artificielle parfaite.&nbsp;»

La révélation venait de s’abattre sur Thomas. Le silence venait de tomber dans la salle. La révélation lui avait fait le même effet que l’un de ces électrochocs que lui administrait O’Connor depuis plusieurs minutes (cela ne faisait-il pas déjà plusieurs heures&nbsp;?). Comment cela se pouvait-il&nbsp;? Comment ne pouvait-il pas être au courant que sa ville était gouvernée par des algorithmes auxquels il avait lui-même participé. En face de lui l’homme aux fines lunettes souriait comme pour savourer une victoire. Il se rendait sans doute compte de l’effet pervers que sa révélation avait eu sur le jeune homme. En avait-il encore en réserve&nbsp;? À moins que…

«&nbsp;Vous mentez&nbsp;», assura Thomas sans ciller.

La douleur frappa de nouveau sans prévenir en se focalisant cette fois-ci dans la tête. Sans s’arrêter un instant, il semblait à Thomas que son cerveau était en train d’emmagasiner une dose importante d’énergie électrique, comme pour s’assurer que sa pensée «&nbsp;impure&nbsp;» ne reviendrait plus. Il voulut crier à O’Connor d’arrêter le supplice mais sa mâchoire ne voulait rien entendre. Ses dents étaient si serrées qu’il pensait les sentir céder sous la pression. La douleur était si intense que ses doigts s’enfonçaient violemment dans la chair de ses paumes. Ses doigts de pieds s’arquaient de nouveau et semblaient pointer dans dix directions différentes. L’énergie qui lui tenaillait les os allait bientôt le faire imploser, Thomas en était certain. La douleur ne s’arrêterait-elle jamais&nbsp;?

Mais elle s’était arrêtée. Depuis combien de temps, il n’en savait rien, mais sa réalité en était plus que palpable, ses mains ensanglantées pouvaient en témoigner et, Thomas en était certain, O’Connor ne viendrait pas remettre en doute cette vérité. L’homme en blouse blanche lui tendait de nouveau sa main en face de la figure. Y avait-il deux doigts tendus, il n’avait aucune certitude là-dessus. Deux, trois, cinq ou cent, ce qu’il voyait lui importait peu, il souhaitait juste échapper encore un peu plus longtemps à la douleur qui peinait à disparaître. Il sentait encore la chaleur brulante lui envahir le crâne et le cerveau. Le sourire insistant de O’Connor attendait une réponse. Deux, trois, cinq ou cent…

«&nbsp;Je ne sais pas ce que je vois. Peu importe ce que je vois, vous me direz toujours que je me trompe.&nbsp;»

Le sourire de O’Connor s’élargit un peu plus.

«&nbsp;Bien, fit-il d’une voix trainante, nous progressons. Nous touchons au but, vous semblez comprendre.  
—&nbsp;Je comprends que vous ne me lâcherez pas.  
—&nbsp;Exact. Nous devons nous assurer que vous êtes effectivement soigné. Qui voudrait d’un déviant se baladant dans les rues de notre magnifique ville&nbsp;?  
—&nbsp;Les Sans-Travails et les junkies&nbsp;», suggéra Thomas sans vraiment y croire.

L’homme partit dans un rire glacial qui se mit à résonner dans la pièce.

«&nbsp;Les bas-fonds de la ville et une vie de paria, c’est de cela dont vous rêvez désormais&nbsp;? Allons, ne souhaiteriez-vous pas vivre heureux et protégé de ces malades qui se croient à l’abri de l’Aigle Blanc&nbsp;?&nbsp;»

C’était effectivement ce qu’il avait toujours voulu. Lui revenait en mémoire ce douloureux souvenir lorsqu’il s’était aventuré dans le sud de la ville à bord du Driver. Il s’était alors fait agresser par une tribu d’hommes et de femmes qui allaient attenter à sa vie, c’était certain&nbsp;! Voilà qu’il avait peur maintenant. O’Connor, face à lui, souriait toujours, éclairant un visage rayonnant. Malgré ce qu’il était en train de lui infliger, Thomas avait la certitude qu’il était en sécurité auprès de lui. Ne l’écoutait-il pas actuellement&nbsp;? N’était-il pas en train de lui expliquer la vérité sur sa vie et ses mensonges&nbsp;? Les larmes lui montaient aux yeux.

«&nbsp;Aidez-moi, pleurnicha-t-il.  
—&nbsp;J’essaye Thomas, mais vous devrez jouer le jeu jusqu’au bout. Voilà ma deuxième question. Attention à ce que vous répondrez, je n’aurai aucun regret à vous punir.&nbsp;»

Thomas acquiesça silencieusement.

«&nbsp;J’aimerais que vous vous concentriez très fort et que vous me racontiez ce dont vous vous souvenez.&nbsp;»

Ce dont il se souvenait&nbsp;? Souvenir de quoi&nbsp;? La question était vague et si large. Il se souvenait de tant de choses qu’il ne savait par où commencer. O’Connor voulait-il en savoir plus sur lui&nbsp;? Il en doutait, il en savait probablement bien plus que Thomas lui-même. Il s’agissait manifestement d’un piège. Il devait faire la part entre ce qu’il avait rêvé et ce qu’il avait réellement vécu. À ce stade de l’interrogatoire, le jeune homme ne savait plus trop ce qu’il devait répondre à part la vérité, toute aussi dangereuse soit-elle. Un léger toussotement lui fit savoir que l’homme en blanc attendait une réponse.

«&nbsp;Je me souviens de tout, répondit-il naïvement. Mon travail, mes amis, où j’habite, ce que je mange. De ma voisine de pallier avec qui j’ai eu des relations comme de la date à laquelle j’ai reçu Preston. Je me souviens de mon emploi du temps quotidien ainsi que des événements de ces derniers jours. Heath Tims, Abie, la résistance ainsi que le visage du junkie qui s’appelait Drew. Je me souviens de tout cela et bien plus encore. Mais ces souvenirs que je visualise parfaitement ne pourraient être que le fruit du jeu de mon cerveau. Qu’il soit malade ou non c’est à lui que je dois ces certitudes dont je n’arrive à me défaire.&nbsp;»

Il avait joué la carte de la sincérité espérant un geste de son tortionnaire bienveillant. Le plissement de ses yeux lui fit comprendre que la réponse satisfaisait l’homme.

Thomas se retrouva toutefois à se remettre d’une énième décharge. Celle-ci avait été aussi foudroyante que violente. L’électricité avait déchiré sa peau et il croyait en sentir des lambeaux se détacher de sa chair. À chaque seconde passée, il sentait son crâne se fendre un peu plus. Les électrodes de chaque côté de sa tête avaient dû fondre se prit à imaginer le jeune homme. Comment pouvaient-elles émettre autant de puissance&nbsp;? Son corps était à bout de force, il sentait à peine ses muscles liquéfiés qui ne bougeaient qu’à la force des décharges qu’on lui injectait. «&nbsp;Une fois de plus et je suis mort&nbsp;», pensa-t-il.

Derrière ses lunettes, O’Connor le regardait. C’était un bel homme&nbsp;; ses lunettes et sa blouse blanche&nbsp;; son sourire et son aura. Thomas l’enviait de se tenir de l’autre côté des électrodes. Il aurait lui-même sans doute pris un certain plaisir à actionner le mécanisme et voir se tordre de douleur un homme qui ne savait plus que penser. Derrière ses lunettes, O’Connor jubilait, ses yeux clairs pétillaient. Il débordait d’excitation à chaque décharge, c’était sa façon à lui de prendre du plaisir. Il y avait un côté érotique dans sa façon de torturer Thomas et ce dernier le ressentait à travers ses yeux mi-clos.

Aussi, lorsque O’Connor actionna une nouvelle fois la machine infernale, Thomas ne put lui en tenir rigueur. Son corps en proie à la fureur électrique semblait doté d’une volonté propre. Ses membres bloqués par les sangles violemment serrées étaient parcourus de spasmes. Il ne sentait plus ni sa mâchoire, ni ses mains qu’il avait serrées tant et si bien qu’elles avaient fini par se confondre dans la douleur. L’odeur de cramé qui envahissait la salle n’avait plus rien d’alarmant pour le sac de chair humaine qui se nommait, encore pour l’heure, Thomas Kell.

«&nbsp;Une dernière fois Thomas, de quoi te souviens-tu&nbsp;?&nbsp;»

Le jeune homme ne se souvenait plus. Il n’y avait dans les limbes de son esprit que de vagues souvenirs décharnés, abandonnés en proie au rapace électrique. Sentiments et émotions fuyaient devant l’abime qui s’offrait à Thomas. La carapace humaine qui se tenait sur la table de verre finit par prononcer quelques mots.

«&nbsp;Je me souviens…  
—&nbsp;Oui&nbsp;? fit O’Connor, visiblement intéressé par ce qui allait suivre.  
—&nbsp;Je me souviens. Blanche. Tunnel.  
—&nbsp;Qu’est-ce qui se trouvait dans le Tunnel&nbsp;?  
—&nbsp;Rose blanche. Pousse.&nbsp;»

Il y avait là, au milieu des décombres de pensées de Thomas, une lueur d’espoir à laquelle il s’accrochait pour vivre. L’ensemble de ses souvenirs avait beau être parti en fumée, il voyait encore nettement cette fleur blanche. Qu’était-elle&nbsp;? Il l’associait à une personne qu’il avait connue, son visage ne lui revenait pas. Il avait cherché le sens de cette fleur il y a longtemps.

«&nbsp;Une rose blanche qui pousse&nbsp;? Dans le Tunnel&nbsp;?&nbsp;», questionna O’Connor, intrigué.

Thomas essayait de rebâtir son esprit autour de cette fleur blanche. Il y avait un tunnel. Il y avait des caisses en bois. De sombres lumières grésillantes éclairant une voie. Un nom. Abie.

«&nbsp;Où est-elle&nbsp;? chuchota Thomas. Où est Abie&nbsp;?  
—&nbsp;Crois-tu connaître Abie&nbsp;? murmura O’Connor dans un souffle à l’oreille du jeune homme. De quoi crois-tu te souvenir&nbsp;? Penses-tu te souvenir t’échapper de chez toi par la fenêtre&nbsp;? Arrives-tu à te rappeler le meurtre de Heath&nbsp;?&nbsp;»

Les questions résonnaient à travers l’esprit ensanglanté de Thomas. À ces questions il arrivait à associer des images naissantes et des sensations perçues à travers son corps. La vue du sang s’écoulant de la gorge de Heath Tims, le froid mordant du vent glacial lors de la fuite avec les Agents ainsi que l’odeur enivrante du vent salé dans le port de la ville sous la Ville. Tout était là mais Thomas n’arrivait plus à y croire pour autant. Tout sonnait faux, grotesque. Cela n’avait pu se passer.

«&nbsp;Oui Thomas, tu commences à prendre conscience de la situation. Ton corps essaye de se souvenir mais ton cerveau meurtri crie désormais à l’abandon. Il commence à lâcher prise, à te laisser entrevoir la vérité. De tout cela il n’y a jamais eu. Non Thomas, tu n’es jamais sorti par cette fenêtre en dégringolant sur le dos d’un Agent. Tu n’as jamais fui cette scène de crime qui n’a jamais existé pour la simple et bonne raison que Heath Tims n’est jamais mort.&nbsp;»

Les mots raisonnaient. Ils raisonnaient avec tant de véracité que Thomas ne pouvait les remettre en doute. Jamais tout cela ne s’était passé, un simple rêve orchestré par son cerveau qui l’avait trahi. Ce cerveau qui s’était extasié pendant des jours et des semaines à s’imaginer tout un monde sous la ville d’Asmara, la ville sous la Ville. Cette résistance qu’il aurait voulu haïr mais qu’il ne pouvait s’empêcher d’admirer sinon comment expliquer le fait que son lâche cerveau ne l’ait façonnée&nbsp;?

«&nbsp;Je… Je l’ai rêvé&nbsp;? Tout cela je l’ai imaginé n’est-ce pas&nbsp;? articula difficilement Thomas.  
—&nbsp;Pas entièrement il est vrai, on t’y a grandement aidé. Mais croyais-tu vraiment pouvoir fuir devant des Agents&nbsp;? Pouvais-tu vraiment croire à cette histoire de résistance capable de s’emparer des robots du Pouvoir afin de les retourner contre leurs véritables maîtres&nbsp;?  
—&nbsp;Mais… Comment&nbsp;?  
—&nbsp;Après que Heath t’ait administré suffisamment d’alcool et de drogue, ton corps ivre ainsi que celui de la jeune fille ont été ramenés dans les locaux de recherche du Pouvoir. Nous vous avons placés tous les deux dans une cuve chargée de stimuler l’ensemble de vos sens pour vous façonner un monde dans lequel vous croyiez vivre. Vos cerveaux se chargeaient de réagir aux stimuli. Alors que vous pensiez fuir, nous observions vos corps et les efforts que faisez vos cerveaux pour trahir lâchement le Pouvoir.&nbsp;»

Chacun des mots de O’Connor avait été prononcés avec une tranquillité déconcertante, tel un grand méchant dévoilant son plan machiavélique au milieu d’un film. Thomas ne pouvait réfuter la vérité telle que présentée par O’Connor car telle était la réalité du Pouvoir.

Les larmes perlaient maintenant aux yeux de Thomas. Il tenta de prononcer un mot que l’homme en blouse ne saisit pas. Il s’approcha du visage du jeune homme qui souffla à nouveau d’une voix brisée&nbsp;:

«&nbsp;Merci.&nbsp;»

Car tel était le sentiment de Thomas. Sincèrement dévoué au Pouvoir, il avait tenté de le trahir. Heath Tims avait probablement repéré ses doutes et le danger que représentait l’homme pour le Pouvoir avant même qu’il ne se rende compte de ses propres pensées. Aussi sincère qu’avait pu être Thomas, son esprit et ses idées lâches avaient essayé de le corrompre pour se retourner contre la ville qu’il aimait tant. Le Pouvoir l’avait sauvé en l’arrêtant à temps et O’Connor, de son regard bienveillant, le protégeait désormais. Tel était le sentiment de l’homme qui gisait maintenant brisé sur une table quelconque au fin fond de l’une des Trois Sœurs. Thomas Kell était devenu un homme dévoué à la réalité, un homme sans rêve.
